jQuery( document ).ready( function( $ ) {
    new ninjaFormsPasswordController()
})

let requirements = [ {
    regex: /^.*(?=.{8,}).*$/,
    message: 'at least 8 characters',
}, {
    regex: /^.*(?=.*[A-Z]).*$/,
    message: 'at least one capital letter',
}, {
    regex: /^.*(?=.*[a-z]).*$/,
    message: 'at least one lower-case letter',
}, {
    regex: /^.*(?=.*\d).*$/,
    message: 'at least one digit',
}, {
    regex: /^.*(?=.*[!@#$%^&*()\-_=+{};:,<.>]).*$/,
    message: 'at least one special symbol like from this set: !@#$%^&*()\-_=+{};:,<.>}',
} ]
var errorID = 'invalid-password'

//console.log(dateFields)
var ninjaFormsPasswordController = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'keyup:field', this.onBlurField )
        this.listenTo( radioFields, 'change:modelValue', this.onChangeModelValue )
    },

    onChangeModelValue: function( model ) {
        var value = model.get( 'value' )
        var fieldID = model.get( 'id' )
        var type = model.get( 'type' )
        var key = model.get('key')

        this.passwordChange( value, fieldID, type, key )
    },

    onBlurField: function( el, model ) {
        var value = jQuery( el ).val()
        var fieldID = model.get( 'id' )
        var type = model.get( 'type' )
        var key = model.get('key')

        this.passwordChange( value, fieldID, type, key )
    },

    passwordChange: function( value, fieldID, type, key ) {
        //console.log(type)
        if(type === 'jg_password' && 'jg_password_1626889919116' === key) {
            if ( 0 < value.length ) {
                let message = []
                let valid = true

                jQuery.each( requirements, function( index ) {
                    // Loop through all regex password requirements
                    let req = requirements[index]

                    //console.log( req )

                    if( req.regex.test( value ) ) {
                        message.push( `<li class="valid">${ req.message }</li>` )
                    } else {
                        message.push( `<li class="invalid">${ req.message }</li>` )
                        valid = false
                    }
                } )
                    nfRadio.channel( 'fields' ).request( 'remove:error', fieldID, errorID )

                if( !valid ) {
                    // Remove error if all requirements are met
                    var fieldModel = nfRadio.channel( 'fields' ).request( 'get:field', fieldID )
                    var formModel  = nfRadio.channel( 'app' ).request( 'get:form',  fieldModel.get( 'formID' ) )

                    message = `<ul class="password-requirements">${ message.join('') }</ul>`

                    nfRadio.channel( 'fields' ).request( 'add:error', fieldID, errorID, message )
                }
            } else {
                // Remove error if field is empty
                nfRadio.channel( 'fields' ).request( 'remove:error', fieldID, errorID )
            }
        }
    },
    
    removeError: function( el, model ) {
        jQuery.each ( requirements, function( index ) {
        } )
    }
});
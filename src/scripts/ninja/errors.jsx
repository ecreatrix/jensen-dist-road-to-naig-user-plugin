jQuery( document ).ready( function( $ ) {
    new ninjaFormsCustomController()
})

var ninjaFormsCustomController = Marionette.Object.extend({
    initialize: function() {
        var fieldsChannel = nfRadio.channel( 'fields' )

        this.listenTo( fieldsChannel, 'blur:field', this.removeError )
        this.listenTo( fieldsChannel, 'change:field', this.removeError )
        this.listenTo( fieldsChannel, 'keyup:field', this.removeError )
    },

    removeError: function( el, model ) {
        nfRadio.channel( 'fields' ).request('remove:error', model.get( 'id' ), 'jg-forms')
    }
});
jQuery( document ).ready( function( $ ) {
    new ninjaFormsUploadsType()
})

var uploadTypeField = { key: 'upload_type_1634666094036', field: false }
var pointsField = { key: 'points_1634666066128', field: false }

var ninjaFormsUploadsType = Marionette.Object.extend({
    initialize: function() {
        let radioFields = nfRadio.channel( 'fields' )

        this.listenTo( radioFields, 'render:view', this.setTypeField )

        this.listenTo( radioFields, 'change:modelValue', this.onChangeModelValue )
    },   

    setTypeField: function( view ) {
        let key = view.model.get( 'key' )

        if ( uploadTypeField.key === key ) 
            uploadTypeField.field = jQuery( view.el ).find( '.nf-element' )

        if ( pointsField.key === key ) 
            pointsField.field = jQuery( view.el ).find( '.nf-element' )
    },

    onChangeModelValue: function( model ) {
        var fieldID = model.get( 'id' )
        var formID = model.get( 'formID' )
        var field = $('#nf-field-' + fieldID + '-container')
        var form = $('.form-' + formID)
        var uploadType = form.data('upload-type')
        var uploadLabel = form.data('upload-label')
        var points = form.data('points')

        if( uploadType && uploadTypeField.field )
            uploadTypeField.field.val( uploadType ).trigger( 'change' )

        if( points && pointsField.field )
            pointsField.field.val( points ).trigger( 'change' )
    },
});
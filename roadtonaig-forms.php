<?php
/**
 * @wordpress-plugin
 * Plugin Name: Road to NAIG Forms
 * Plugin URI:  https://jensengroup.ca/
 * Description: Manipulate Ninja forms results
 * Version:     2021.7
 * Author:      Jensen Group
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 * @package   jg-Gutenberg
 *
 * @author    Jensen Group
 * @copyright 2021 Jensen Group
 * @license   GPL-2.0+
 */
$files = [
    'assets',
    'ninja/setup', 'ninja/helpers/fields', 'ninja/config',
    'ninja/action/family', 'ninja/action/password', 'ninja/action/register', 'ninja/action/uploads',
    'ninja/default/family', 'ninja/default/profile', 'ninja/default/uploads',
    'ninja/fields/password',
];
foreach ( $files as $file ) {
    $file = plugin_dir_path( __FILE__ ) . 'app/' . $file . '.php';

    if ( file_exists( $file ) ) {
        require $file;
    }
}

add_filter( 'ninja_forms_register_fields', 'register_fields' );
function register_fields( $fields ) {
    $fields['jg_password'] = new NF_Password();

    return $fields;
}

/******/ (function() { // webpackBootstrap
/******/ 	var __webpack_modules__ = ({

/***/ "./roadtonaig-forms/src/scripts/ninja.jsx":
/*!************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja.jsx ***!
  \************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ninja_errors__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ninja/errors */ "./roadtonaig-forms/src/scripts/ninja/errors.jsx");
/* harmony import */ var _ninja_errors__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_ninja_errors__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _ninja_age__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./ninja/age */ "./roadtonaig-forms/src/scripts/ninja/age.jsx");
/* harmony import */ var _ninja_age__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_ninja_age__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _ninja_emailConfirmProfile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./ninja/emailConfirmProfile */ "./roadtonaig-forms/src/scripts/ninja/emailConfirmProfile.jsx");
/* harmony import */ var _ninja_emailConfirmProfile__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_ninja_emailConfirmProfile__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ninja_password__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ninja/password */ "./roadtonaig-forms/src/scripts/ninja/password.jsx");
/* harmony import */ var _ninja_password__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_ninja_password__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _ninja_uploads__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ninja/uploads */ "./roadtonaig-forms/src/scripts/ninja/uploads.jsx");
/* harmony import */ var _ninja_uploads__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(_ninja_uploads__WEBPACK_IMPORTED_MODULE_4__);






/***/ }),

/***/ "./roadtonaig-forms/src/scripts/ninja/age.jsx":
/*!****************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja/age.jsx ***!
  \****************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsAgeController();
});
var dateFields = {
  main: {
    key: 'dob',
    field: false
  },
  child1: {
    key: 'child_1_dob',
    field: false
  },
  child2: {
    key: 'child_2_dob',
    field: false
  },
  child3: {
    key: 'child_3_dob',
    field: false
  },
  child4: {
    key: 'child_4_dob',
    field: false
  }
};
var errorID = 'invalid-age';
var error_profile_message = '';
var error_child_message = '';
var ninjaFormsAgeController = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'render:view', this.setAgeFields);
    this.listenTo(radioFields, 'keyup:field', this.onBlurField);
    this.listenTo(radioFields, 'change:modelValue', this.onChangeModelValue);
  },
  setAgeFields: function setAgeFields(view) {
    var type = view.model.get('type');
    var key = view.model.get('key');
    var container_class = view.model.get('container_class');
    var value = view.model.get('value');

    if ('html' === type && ~container_class.indexOf('error-profile-message')) {
      error_profile_message = value;
    } else if ('html' === type && ~container_class.indexOf('error-youth-message')) {
      error_child_message = value;
    }

    jQuery.each(dateFields, function (index) {
      var element = dateFields[index];
      if (element.key === key) dateFields[index].field = jQuery(view.el).find('.nf-element');
    });
  },
  onChangeModelValue: function onChangeModelValue(model) {
    var type = model.get('type');

    if (type === 'date') {
      var date = model.get('value');
      var fieldID = model.get('id');
      var category = model.get('container_class');
      this.dateChange(date, fieldID, category);
    }
  },
  onBlurField: function onBlurField(el, model) {
    var type = model.get('type');

    if (type === 'date') {
      var date = model.get('value');
      var fieldID = model.get('id');
      var category = model.get('container_class');
      this.dateChange(date, fieldID, category);
    }
  },
  dateChange: function dateChange(date, fieldID, category) {
    if (0 < date.length) {
      var difference = new Date(new Date() - new Date(date)).getFullYear() - 1970; //console.log(difference)

      if (difference >= 10 && difference <= 19) {
        nfRadio.channel('fields').request('remove:error', fieldID, errorID);
      } else {
        if ('profile' === category) {
          var fieldModel = nfRadio.channel('fields').request('get:field', fieldID);
          var formModel = nfRadio.channel('app').request('get:form', fieldModel.get('formID'));
          nfRadio.channel('fields').request('add:error', fieldID, errorID, error_profile_message);
        } else {
          var fieldModel = nfRadio.channel('fields').request('get:field', fieldID);
          var formModel = nfRadio.channel('app').request('get:form', fieldModel.get('formID'));
          nfRadio.channel('fields').request('add:error', fieldID, errorID, error_child_message);
        }
      }
    }
  }
});

/***/ }),

/***/ "./roadtonaig-forms/src/scripts/ninja/emailConfirmProfile.jsx":
/*!********************************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja/emailConfirmProfile.jsx ***!
  \********************************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsEmailConfirmController();
});
var email = {
  key: 'email_1625951020517',
  field: false
};
var emailConfirm = {
  key: 'email_confirmation_1625951090593',
  field: false
};
var userID = false;
var ninjaFormsEmailConfirmController = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'render:view', this.setConfirmFields);
    this.listenTo(radioFields, 'blur:field', this.updateEmailConfirm);
    this.listenTo(radioFields, 'change:field', this.updateEmailConfirm);
    this.listenTo(radioFields, 'keyup:field', this.updateEmailConfirm);
  },
  setConfirmFields: function setConfirmFields(view) {
    var key = view.model.get('key');
    if (email.key === key) email.field = jQuery(view.el).find('.nf-element');
    if (emailConfirm.key === key) emailConfirm.field = jQuery(view.el).find('.nf-element');
    if ('query_user_id' === key) userID = jQuery(view.el).find('.nf-element').val();
  },
  // Match email field when used on profile page by logged in users
  updateEmailConfirm: function updateEmailConfirm(el, model) {
    var key = model.get('key');
    var value = model.get('value');

    if (userID && email.key === key) {
      emailConfirm.field.val(value).trigger('change');
    }
  }
});

/***/ }),

/***/ "./roadtonaig-forms/src/scripts/ninja/errors.jsx":
/*!*******************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja/errors.jsx ***!
  \*******************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsCustomController();
});
var ninjaFormsCustomController = Marionette.Object.extend({
  initialize: function initialize() {
    var fieldsChannel = nfRadio.channel('fields');
    this.listenTo(fieldsChannel, 'blur:field', this.removeError);
    this.listenTo(fieldsChannel, 'change:field', this.removeError);
    this.listenTo(fieldsChannel, 'keyup:field', this.removeError);
  },
  removeError: function removeError(el, model) {
    nfRadio.channel('fields').request('remove:error', model.get('id'), 'jg-forms');
  }
});

/***/ }),

/***/ "./roadtonaig-forms/src/scripts/ninja/password.jsx":
/*!*********************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja/password.jsx ***!
  \*********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsPasswordController();
});
var requirements = [{
  regex: /^.*(?=.{8,}).*$/,
  message: 'at least 8 characters'
}, {
  regex: /^.*(?=.*[A-Z]).*$/,
  message: 'at least one capital letter'
}, {
  regex: /^.*(?=.*[a-z]).*$/,
  message: 'at least one lower-case letter'
}, {
  regex: /^.*(?=.*\d).*$/,
  message: 'at least one digit'
}, {
  regex: /^.*(?=.*[!@#$%^&*()\-_=+{};:,<.>]).*$/,
  message: 'at least one special symbol like from this set: !@#$%^&*()\-_=+{};:,<.>}'
}];
var errorID = 'invalid-password'; //console.log(dateFields)

var ninjaFormsPasswordController = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'keyup:field', this.onBlurField);
    this.listenTo(radioFields, 'change:modelValue', this.onChangeModelValue);
  },
  onChangeModelValue: function onChangeModelValue(model) {
    var value = model.get('value');
    var fieldID = model.get('id');
    var type = model.get('type');
    var key = model.get('key');
    this.passwordChange(value, fieldID, type, key);
  },
  onBlurField: function onBlurField(el, model) {
    var value = jQuery(el).val();
    var fieldID = model.get('id');
    var type = model.get('type');
    var key = model.get('key');
    this.passwordChange(value, fieldID, type, key);
  },
  passwordChange: function passwordChange(value, fieldID, type, key) {
    //console.log(type)
    if (type === 'jg_password' && 'jg_password_1626889919116' === key) {
      if (0 < value.length) {
        var message = [];
        var valid = true;
        jQuery.each(requirements, function (index) {
          // Loop through all regex password requirements
          var req = requirements[index]; //console.log( req )

          if (req.regex.test(value)) {
            message.push("<li class=\"valid\">".concat(req.message, "</li>"));
          } else {
            message.push("<li class=\"invalid\">".concat(req.message, "</li>"));
            valid = false;
          }
        });
        nfRadio.channel('fields').request('remove:error', fieldID, errorID);

        if (!valid) {
          // Remove error if all requirements are met
          var fieldModel = nfRadio.channel('fields').request('get:field', fieldID);
          var formModel = nfRadio.channel('app').request('get:form', fieldModel.get('formID'));
          message = "<ul class=\"password-requirements\">".concat(message.join(''), "</ul>");
          nfRadio.channel('fields').request('add:error', fieldID, errorID, message);
        }
      } else {
        // Remove error if field is empty
        nfRadio.channel('fields').request('remove:error', fieldID, errorID);
      }
    }
  },
  removeError: function removeError(el, model) {
    jQuery.each(requirements, function (index) {});
  }
});

/***/ }),

/***/ "./roadtonaig-forms/src/scripts/ninja/uploads.jsx":
/*!********************************************************!*\
  !*** ./roadtonaig-forms/src/scripts/ninja/uploads.jsx ***!
  \********************************************************/
/***/ (function() {

jQuery(document).ready(function ($) {
  new ninjaFormsUploadsType();
});
var uploadTypeField = {
  key: 'upload_type_1634666094036',
  field: false
};
var pointsField = {
  key: 'points_1634666066128',
  field: false
};
var ninjaFormsUploadsType = Marionette.Object.extend({
  initialize: function initialize() {
    var radioFields = nfRadio.channel('fields');
    this.listenTo(radioFields, 'render:view', this.setTypeField);
    this.listenTo(radioFields, 'change:modelValue', this.onChangeModelValue);
  },
  setTypeField: function setTypeField(view) {
    var key = view.model.get('key');
    if (uploadTypeField.key === key) uploadTypeField.field = jQuery(view.el).find('.nf-element');
    if (pointsField.key === key) pointsField.field = jQuery(view.el).find('.nf-element');
  },
  onChangeModelValue: function onChangeModelValue(model) {
    var fieldID = model.get('id');
    var formID = model.get('formID');
    var field = $('#nf-field-' + fieldID + '-container');
    var form = $('.form-' + formID);
    var uploadType = form.data('upload-type');
    var uploadLabel = form.data('upload-label');
    var points = form.data('points');
    if (uploadType && uploadTypeField.field) uploadTypeField.field.val(uploadType).trigger('change');
    if (points && pointsField.field) pointsField.field.val(points).trigger('change');
  }
});

/***/ }),

/***/ "./roadtonaig-forms/src/styles/app.scss":
/*!**********************************************!*\
  !*** ./roadtonaig-forms/src/styles/app.scss ***!
  \**********************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	!function() {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = function(result, chunkIds, fn, priority) {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var chunkIds = deferred[i][0];
/******/ 				var fn = deferred[i][1];
/******/ 				var priority = deferred[i][2];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every(function(key) { return __webpack_require__.O[key](chunkIds[j]); })) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	!function() {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = function(module) {
/******/ 			var getter = module && module.__esModule ?
/******/ 				function() { return module['default']; } :
/******/ 				function() { return module; };
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	!function() {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = function(exports, definition) {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	!function() {
/******/ 		__webpack_require__.o = function(obj, prop) { return Object.prototype.hasOwnProperty.call(obj, prop); }
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	!function() {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = function(exports) {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	}();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	!function() {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/assets/scripts/ninja": 0,
/******/ 			"assets/styles/app": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = function(chunkId) { return installedChunks[chunkId] === 0; };
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = function(parentChunkLoadingFunction, data) {
/******/ 			var chunkIds = data[0];
/******/ 			var moreModules = data[1];
/******/ 			var runtime = data[2];
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some(function(id) { return installedChunks[id] !== 0; })) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkIds[i]] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunkjg_road_to_naig"] = self["webpackChunkjg_road_to_naig"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	}();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["assets/styles/app"], function() { return __webpack_require__("./roadtonaig-forms/src/scripts/ninja.jsx"); })
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["assets/styles/app"], function() { return __webpack_require__("./roadtonaig-forms/src/styles/app.scss"); })
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;
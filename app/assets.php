<?php
namespace jg\Plugin\User;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Assets::class ) ) {
	class Assets {
		private $prefix = 'user';

		public function __construct() {
			add_action( 'wp_enqueue_scripts', [$this, 'frontend'] );
			add_action( 'wp_enqueue_scripts', [$this, 'frontend_ninja'], 999 );
		}

		// Enqueue local and remote assets
		public function add_assets( $assets = false, $type = false ) {
			if ( ! $assets || ! is_array( $assets ) ) {
				return;
			}

			foreach ( $assets as $item ) {
				// Name required, skip if it is not present
				if ( ! array_key_exists( 'name', $item ) ) {
					continue;
				}

				$name  = $item['name'];
				$asset = null;

				// Figure out if it's a script or a style
				$script = false;
				$style  = false;
				if ( 'js' === $type || strpos( $name, '.js' ) !== false ) {
					$script = true;
				} else if ( 'css' === $type || strpos( $name, '.css' ) !== false ) {
					$style = true;
				}

				// Get local file or remote link
				if ( array_key_exists( 'link', $item ) && strpos( $item['link'], 'http' ) !== false ) {
					$asset = $item['link'];
				} else if ( $style ) {
					$asset = 'assets/styles/' . $item['name'] . '.css';
				} else if ( $script ) {
					$asset = 'assets/scripts/' . $item['name'] . '.js';
				}

				// Set version for cache busting
				$version     = null;
				$path_prefix = plugin_dir_path( __DIR__ );
				$uri_prefix  = plugin_dir_url( __DIR__ );
				if ( file_exists( $path_prefix . $asset ) ) {
					$version = filemtime( $path_prefix . $asset );
					$uri     = $uri_prefix . $asset;
				} else if ( array_key_exists( 'link', $item ) ) {
					$uri = $item['link'];
				} else {
					return;
				}

				$dependencies = [];
				if ( array_key_exists( 'dependencies', $item ) && is_array( $item['dependencies'] ) ) {
					$dependencies = $item['dependencies'];
				}

				// Add prefix before name
				$name = 'jg-user-' . $name;
				if ( array_key_exists( 'prefix', $item ) ) {
					$name = $item['prefix'] . $name;
				}

				// Add parameters inline for use by scripts
				if ( array_key_exists( 'script_object', $item ) && array_key_exists( 'parameters', $item ) ) {
					wp_localize_script( $name, $item['script_object'], $item['parameters'] );
				}

				// Enqueue file/link
				if ( $script ) {
					wp_enqueue_script( $name, $uri, $dependencies, $version, true );
				} else if ( $style ) {
					wp_enqueue_style( $name, $uri, $dependencies, $version );
				}
			}
		}

		public function frontend() {
			$scripts = [
				'main' => [
					'name'         => 'app',
					'dependencies' => ['jquery', 'wp-api-fetch'],
				],
			];

			$this->add_assets( $scripts, 'js', $this->prefix );

			$styles = [
				[
					'name' => 'app',
				],
			];

			$this->add_assets( $styles, 'css', $this->prefix );
		}

		public function frontend_ninja() {
			$scripts = [
				'main' => [
					'name'         => 'ninja',
					'dependencies' => ['jquery', 'nf-front-end-deps'],
				],
			];

			$this->add_assets( $scripts, 'js', $this->prefix );
		}
	}

	new Assets();
}
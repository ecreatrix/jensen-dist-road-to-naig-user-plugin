<?php

class NF_Password extends NF_Fields_Textbox {
    protected $_icon = 'unlock-alt';

    protected $_name = 'jg_password';

    protected $_section = 'userinfo';

    protected $_templates = 'textbox';

    protected $_type = 'textbox';

    public function __construct() {
        parent::__construct();
        $this->_nicename = __( 'Password', 'ninja-forms-vimeo-uploader' );
    }
}
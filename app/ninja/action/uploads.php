<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionUploads::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionUploads extends \NF_Abstracts_Action {
        protected $_name = 'jg-uploads';

        protected $_nicename = 'Uploads';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::uploads();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        // Field ID, Field Value, Field action
        public function process( $action_values, $form_id, $data ) {
            $helpersFields = new NinjaHelpersFields();
            $helpersUser   = new \jg\Theme\HelpersUser();

            $fields = $helpersFields->get_fields( $form_id, array_flip( $action_values ), $this->_name );

            $wp_post_id = $fields['post_id']['value'];

            $query_user_id = $fields['query_user_id']['value'];
            $wp_user_id    = $fields['wp_user_id']['value'];
            $user_id       = $query_user_id ? $query_user_id : $wp_user_id;

            $upload_type = $fields['upload_type']['value'];
            $points      = $fields['points']['value'];

            $submember      = $fields['submember']['value'];
            $attachment_url = $data['fields'][$fields['file_upload']['id']]['files'][0]['data']['file_url'];

            if ( ! is_array( $uploads ) ) {
                $uploads = [];
            }

            $family = $helpersUser::get_family( $user_id );

            $edition      = \jg\Theme\HelpersUser::get_edition();
            $edition_info = get_user_meta( $user_id, $edition, true );

            $points_meta = is_array( $edition_info ) && array_key_exists( 'points', $edition_info ) ? $edition_info['points'] : 0;

            $uploads_key  = $edition . '-' . $wp_post_id . '-' . $upload_type;
            $uploads_meta = is_array( $edition_info ) && array_key_exists( 'uploads', $edition_info ) ? $edition_info['uploads'] : 0;

            if ( $attachment_url ) {
                $new_upload = ['url' => $attachment_url, 'points' => $points, 'post' => $wp_post_id, 'upload_type' => $upload_type];

                if ( $family ) {
                    // Family type
                    // Set submember info
                    $submember_points_meta = $edition_info[$submember]['points'];

                    if ( ! is_array( $family[$submember] ) || ! is_array( $family[$submember][$edition]['uploads'] ) ) {
                        // Submember does not have uploads array for this edition
                        $family[$submember][$edition]['uploads'] = [];
                    }

                    $submember_uploads_meta = $family[$submember][$edition]['uploads'];

                    if ( ! array_key_exists( $uploads_key, $submember_uploads_meta ) ) {
                        // This upload type does not exist in user, add points and add attachment
                        $family[$submember][$edition]['points'] = $submember_points_meta + $points;
                        $edition_info['points']                 = $points_meta + $points;
                    }

                    $family[$submember][$edition]['uploads'][$uploads_key] = $new_upload;

                    update_user_meta( $user_id, 'family', $family );
                } else {
                    // Youth Type
                    if ( ! is_array( $edition_info ) || ! is_array( $edition_info['uploads'] ) ) {
                        // User does not have uploads array for this edition
                        $edition_info['uploads'] = [];
                    }

                    if ( ! array_key_exists( $uploads_key, $uploads_meta ) ) {
                        // This upload type does not exist in user, add points and add attachment
                        $edition_info['points'] = $points_meta + $points;
                    }

                    $edition_info['uploads'][$uploads_key] = $new_upload;
                }
            }

            update_user_meta( $user_id, 'points', $points_meta );
            update_user_meta( $user_id, $edition, $edition_info );

            /*$data['errors']['form'] = [
            'message' => print_r( get_user_meta( $user_id, $edition, true ), true ),
            'slug'    => 'jg-forms',
            ];*/

            return $data;
        }
    }
}

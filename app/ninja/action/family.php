<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionFamilyMembers::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionFamilyMembers extends \NF_Abstracts_Action {
        protected $_name = 'jg-family-member';

        protected $_nicename = 'Add Family Member';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::family();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        public function process( $action_values, $form_id, $data ) {
            $helpersFields = new NinjaHelpersFields();
            $helpersUser   = new \jg\Theme\HelpersUser();

            $fields = $helpersFields->get_fields( $form_id, array_flip( $action_values ), $this->_name );

            $query_post_id = $fields['query_post_id']['value'];

            $query_user_id = $fields['query_user_id']['value'];
            $wp_user_id    = $fields['wp_user_id']['value'];
            $user_id       = $query_user_id ? $query_user_id : $wp_user_id;

            $query_submember_id = $fields['query_submember_id']['value'];

            $current_meta = $helpersUser->current_meta( $user_id );

            $meta = $helpersUser->userdata_individual( $fields, $current_meta, 'child1_', $query_submember_id );
            update_user_meta( $user_id, 'family', $meta['family'] );

            $data['actions']['redirect'] = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'profile' )['permalink'];

            return $data;
        }
    }
}
<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionRegister::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionRegister extends \NF_Abstracts_Action {
        protected $_name = 'jg-register-user';

        protected $_nicename = 'Register User';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::register_user();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        // Field ID, Field Value, Field action
        public function process( $action_values, $form_id, $data ) {
            $helpersFields = new NinjaHelpersFields();
            $helpersUser   = new \jg\Theme\HelpersUser();

            $fields = $helpersFields->get_fields( $form_id, array_flip( $action_values ), $this->_name );

            $query_post_id = $fields['query_post_id']['value'];

            $query_user_id = $fields['query_user_id']['value'];
            $wp_user_id    = $fields['wp_user_id']['value'];
            $user_id       = $query_user_id ? $query_user_id : $wp_user_id;

            $username        = $fields['username']['value'];
            $username        = sanitize_user( trim( esc_attr( $username ) ) );
            $username_exists = username_exists( $username );

            $email        = apply_filters( 'user_registration_email', esc_attr( $fields['email']['value'] ) );
            $email_exists = email_exists( $email );

            $current_password = $fields['current_password']['value'];

            $firstname = $fields['first_name']['value'];
            $lastname  = $fields['last_name']['value'];

            $new_password = $fields['new_password']['value'];
            //$user_id = $helpersUser->get_user_id();

            if ( $user_id ) {
                // If user is already logged in, check against current values
                $userdata = get_user_by( 'id', $user_id );

                $username_exists = $userdata->user_login !== $username && $username_exists;
                $email_exists    = $userdata->user_email !== $email && $email_exists;

                $password_check = wp_check_password( $current_password, $userdata->user_pass, $user_id );

                if ( ! $password_check ) {
                    $data['errors']['fields'][$fields['current_password']['id']] = [
                        'message' => 'Wrong password. Please enter your current password.',
                        'slug'    => 'jg-forms',
                    ];

                    return $data;
                }
            }

            if ( $username && $username_exists ) {
                $data['errors']['fields'][$fields['username']['id']] = [
                    'message' => $helpersFields->error_in_use( 'username' ),
                    'slug'    => 'jg-forms',
                ];
            }

            if ( $username && strlen( $username ) < 3 ) {
                $data['errors']['fields'][$fields['username']['id']] = [
                    'message' => 'Please choose a username that is at least <u>3</u> characters long.' . $username,
                    'slug'    => 'jg-forms',
                ];

                return $data;
            }

            if ( $email_exists ) {
                $data['errors']['fields'][$fields['email']['id']] = [
                    'message' => $helpersFields->error_in_use( 'email' ),
                    'slug'    => 'jg-forms',
                ];
            }

            if ( $username_exists || $email_exists ) {
                return $data;
            }

            $userdata = $helpersUser->userdata_profile( $fields, $user_id );

            if ( ! is_user_logged_in() ) {
                $user_id = wp_insert_user( $userdata );
                $helpersUser->update_password( $user_id, $username, $new_password );

                if ( ! $user_id ) {
                    $data['errors']['form'] = [
                        'message' => 'An error has occured, please contact us for help.',
                        'slug'    => 'jg-forms',
                    ];

                    return $data;
                }
            }
            $userdata['ID'] = $user_id;

            $meta = $helpersUser->family_meta( $fields, $userdata );

            $profile_page      = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'profile' );
            $registration_page = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'registration_start' );

            if ( is_user_logged_in() ) {
                global $current_user;

                if ( $profile_page['current_page'] && ! in_array( 'administrator', $current_user->roles ) ) {
                    $data['actions']['redirect'] = $profile_page['permalink'] . $profile_page['add_query_user'];
                } else if ( $registration_page['current_page'] && ! in_array( 'administrator', $current_user->roles ) ) {
                    $data['actions']['redirect'] = $profile_page['permalink'] . $profile_page['add_query_user'];
                }
            } else if ( $registration_page['current_page'] ) {
                $data['actions']['redirect'] = $registration_page['permalink'] . $registration_page['add_query_user'];
            }

            return $data;
        }
    }
}

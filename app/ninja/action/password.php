<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaActionPassword::class ) && class_exists( 'NF_Abstracts_Action' ) ) {
    class NinjaActionPassword extends \NF_Abstracts_Action {
        protected $_name = 'jg-password';

        protected $_nicename = 'Update password';

        protected $_priority = '10';

        protected $_tags = [];

        protected $_timing = 'normal';

        public function __construct() {
            parent::__construct();

            add_action( 'admin_init', [$this, 'init_settings'] );
        }

        public function init_settings() {
            $settings = NinjaConfig::update_password();

            $this->_settings = array_merge( $this->_settings, $settings );
        }

        // Field ID, Field Value, Field action
        public function process( $action_values, $form_id, $data ) {
            $helpersFields = new NinjaHelpersFields();
            $helpersUser   = new \jg\Theme\HelpersUser();

            $fields = $helpersFields->get_fields( $form_id, array_flip( $action_values ), $this->_name );

            $query_post_id = $fields['wp_post_id']['value'];

            $query_user_id = $fields['query_user_id']['value'];
            $wp_user_id    = $fields['wp_user_id']['value'];
            $user_id       = $query_user_id ? $query_user_id : $wp_user_id;

            $new_password     = $fields['new_password']['value'];
            $current_password = $fields['current_password']['value'];

            $userdata = get_user_by( 'id', $user_id );
            $username = $userdata->user_login;

            $password_check = wp_check_password( $current_password, $userdata->user_pass, $user_id );

            if ( ! $password_check ) {
                $data['errors']['fields'][$fields['current_password']['id']] = [
                    'message' => 'Wrong password. Please enter your current password.',
                    'slug'    => 'jg-forms',
                ];

                return $data;
            }

            $helpersUser->update_password( $user_id, $username, $new_password );

            $profile_page      = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'profile' );
            $registration_page = \jg\Theme\HelpersTheme::theme_page( $query_post_id, 'registration_start' );

            if ( is_user_logged_in() ) {
                if ( $profile_page['current_page'] ) {
                    $data['actions']['redirect'] = $profile_page['permalink'] . $profile_page['add_query_user'];
                } else if ( $registration_page['current_page'] ) {
                    $data['actions']['redirect'] = $profile_page['permalink'] . $profile_page['add_query_user'];
                }
            } else if ( $registration_page['current_page'] ) {
                $data['actions']['redirect'] = $registration_page['permalink'] . $registration_page['add_query_user'];
            }

            return $data;
        }
    }
}

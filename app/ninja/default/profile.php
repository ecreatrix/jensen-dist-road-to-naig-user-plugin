<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaDefaultsProfile::class ) ) {
	class NinjaDefaultsProfile {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids       = $field_ids;

			add_filter( 'ninja_forms_render_default_value', [$this, 'pre_populate_field_data'], 10, 3 );
			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );
		}

		public function pre_populate_field_data( $default_value, $field_type, $settings ) {
			$helpersFields = new NinjaHelpersFields();
			$helpersUser   = new \jg\Theme\HelpersUser();

			$field_key  = $settings['key'];
			$action_key = $this->_field_ids[$field_key];

			if ( 'jg-register-user' === $this->_action_settings['type'] && $action_key ) {
				$user_id   = $helpersUser->get_user_id();
				$user_data = get_userdata( $user_id );

				switch ( $action_key ) {
					case 'email':
					case 'email_confirmation':
						$default_value = $user_data->user_email;
						break;
					case 'username':
						$default_value = $user_data->user_login;
						break;
					case 'dob':
						$default_value = $user_data->dob;
						break;
					case 'first_name':
						$default_value = $user_data->first_name;
						break;
					case 'last_name':
						$default_value = $user_data->last_name;
						break;
					case 'street1':
						$default_value = $user_data->street1;
						break;
					case 'city1':
						$default_value = $user_data->city1;
						break;
					case 'postal_code1':
						$default_value = $user_data->postal_code1;
						break;
					default:
						/*if ( 'html' !== $field_type ) {
						$meta = get_user_meta( $user_id, $action_key, true );

						if ( $meta ) {
						$default_value = $meta;
						}
						}*/
						break;
				}
			}

			return $default_value;
		}

		// Set select/checkmarks to saved user value
		public function render_options( $options, $settings ) {
			$helpersUser = new \jg\Theme\HelpersUser();
			$user_id     = $helpersUser->get_user_id();

			$field_key  = $settings['key'];
			$action_key = $this->_field_ids[$field_key];

			if ( 'jg-register-user' === $this->_action_settings['type'] && $action_key ) {
				$user_meta = get_user_meta( $user_id, $action_key, true );

				if ( ! $user_meta ) {
					return $options;
				}
				foreach ( $options as $key => $option ) {
					if ( $option['value'] && $user_meta === $option['value'] ) {
						$options[$key]['selected'] = 1;
					} else {
						$options[$key]['selected'] = 0;
					}

				}
			}

			return $options;
		}
	}
}

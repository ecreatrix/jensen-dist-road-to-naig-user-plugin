<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaDefaultsUploads::class ) ) {
	class NinjaDefaultsUploads {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids       = $field_ids;

			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );

		}

		// Set select/checkmarks to saved user value
		public function render_options( $options, $settings ) {
			$helpersUser = new \jg\Theme\HelpersUser();

			$field_key  = $settings['key'];
			$action_key = $this->_field_ids[$field_key];

			if ( 'jg-uploads' === $this->_action_settings['type'] && 'submember' === $action_key ) {
				$user_id = $helpersUser->get_user_id();
				$family  = $helpersUser::get_family( $user_id );

				if ( $family ) {
					$members = $helpersUser::family_select_field( $user_id );

					$options[0]          = $options[0];
					$options[0]['label'] = 'Please Make a Selection';
					$options[0]['value'] = '';

					$default_value = '';

					$i = 1;
					foreach ( $members as $key => $member ) {
						$options[$i]          = $options[0];
						$options[$i]['label'] = $member;
						$options[$i]['value'] = $key;

						$family[$key]['uploads'] = false;
						$family[$key]['points']  = 0;

						$i++;
					}
				} else {
					$default_value = "youth";
				}

				foreach ( $options as $key => $option ) {
					if ( $option['value'] && $default_value === $option['value'] ) {
						$options[0]['selected']    = false;
						$options[$key]['selected'] = 1;
					}
				}
			}

			return $options;
		}
	}
}

<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! class_exists( NinjaDefaultsFamily::class ) ) {
	class NinjaDefaultsFamily {
		private $_action_settings;

		private $_field_ids;

		public function __construct( $action_settings, $field_ids ) {
			$this->_action_settings = $action_settings;
			$this->_field_ids       = $field_ids;

			add_filter( 'ninja_forms_render_default_value', [$this, 'pre_populate_field_data'], 10, 3 );
			add_filter( 'ninja_forms_render_options', [$this, 'render_options'], 10, 2 );

			//add_filter( 'ninja_forms_after_upgrade_settings', [$this, 'upgrade_field_settings'] );
		}

		public function pre_populate_field_data( $default_value, $field_type, $settings ) {
			$helpersUser = new \jg\Theme\HelpersUser();

			$field_key           = $settings['key'];
			$action_key          = $this->_field_ids[$field_key];
			$modify_type         = $_GET['type'];
			$modify_submember_id = $_GET['id'];

			if ( 'jg-family-member' === $this->_action_settings['type'] && $action_key && $modify_type && $modify_submember_id ) {
				$user_id = $helpersUser->get_user_id();

				$family = $helpersUser::get_family( $user_id );
				if ( ! $family ) {
					return $default_value;
				}

				$submember = $family[$modify_submember_id];

				if ( ! $submember ) {
					return $default_value;
				}

				switch ( $action_key ) {
					case 'child1_first_name':
						$default_value = $submember['first_name'];
						break;
					case 'child1_last_name':
						$default_value = $submember['last_name'];
						break;
				}
			}

			return $default_value;
		}

		// Set select/checkmarks to saved user value
		public function render_options( $options, $settings ) {
			$helpersUser = new \jg\Theme\HelpersUser();

			$field_key           = $settings['key'];
			$action_key          = $this->_field_ids[$field_key];
			$modify_type         = $_GET['type'];
			$modify_submember_id = $_GET['id'];

			if ( 'jg-family-member' === $this->_action_settings['type'] && 'child1_shirt_size' === $action_key && $modify_type && $modify_submember_id ) {
				$user_id = $helpersUser->get_user_id();

				$family = $helpersUser::get_family( $user_id );

				if ( ! $family ) {
					return $options;
				}
				$submember = $family[$modify_submember_id];

				if ( ! $submember ) {
					return $options;
				}

				$default_value = $submember['shirt_size'];

				foreach ( $options as $key => $option ) {
					if ( $option['value'] && $default_value === $option['value'] ) {
						$options[0]['selected']    = false;
						$options[$key]['selected'] = 1;
					}
				}
			}

			return $options;
		}
	}
}

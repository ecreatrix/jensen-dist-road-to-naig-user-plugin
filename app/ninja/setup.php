<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaSetup::class ) ) {
    class NinjaSetup {
        private static $instance;

        public function __construct() {
            add_filter( 'ninja_forms_register_actions', [$this, 'register_actions'] );
            add_action( 'nf_get_form_id', [$this, 'default_values'] );
        }

        public function default_values( $form_id ) {
            $actions = Ninja_Forms()->form( $form_id )->get_actions();

            foreach ( $actions as $action ) {
                $action_type     = $action->get_setting( 'type' );
                $action_settings = $action->get_settings();
                $field_ids       = array_flip( NinjaHelpersFields::strip_merge_tags( $action_settings ) );

                if ( 'jg-register-user' === $action_type ) {
                    new NinjaDefaultsProfile( $action_settings, $field_ids );
                } else if ( 'jg-family-member' === $action_type ) {
                    new NinjaDefaultsFamily( $action_settings, $field_ids );
                } else if ( 'jg-uploads' === $action_type ) {
                    new NinjaDefaultsUploads( $action_settings, $field_ids );
                }
            }
        }

        //Get the field key of each field in the fields array.
        public function get_field_id( $fields, $settings ) {
            $field_ids = [];

            foreach ( $fields as $field ) {
                $field_key = $field->get_setting( 'key' );

                foreach ( $settings as $setting_key => $setting_value ) {
                    if ( $setting_key == $field_key ) {
                        $field_ids[$setting_value] = $field->get_id();
                    }
                }
            }

            return $field_ids;
        }

        public static function instance() {
            if ( ! isset( self::$instance ) && ! ( self::$instance instanceof self ) ) {
                self::$instance = new self();

            }

            return self::$instance;
        }

        public function register_actions( $actions ) {
            $actions['jg-password']      = new NinjaActionPassword();
            $actions['jg-register-user'] = new NinjaActionRegister();
            $actions['jg-family-member'] = new NinjaActionFamilyMembers();
            $actions['jg-uploads']       = new NinjaActionUploads();

            return $actions;
        }
    }

    function NinjaSetup() {
        return NinjaSetup::instance();
    }

    NinjaSetup();
}
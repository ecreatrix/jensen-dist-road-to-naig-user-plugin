<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaConfig::class ) ) {
    class NinjaConfig {
        public function __construct() {
            apply_filters( 'ninja_forms_register_user_settings', [$this, 'register_user'] );
            apply_filters( 'ninja_forms_family_settings', [$this, 'family'] );
            apply_filters( 'ninja_forms_post_uploads_settings', [$this, 'uploads'] );
        }

        public static function add_kids( $num = 4, $new_member = true ) {
            $children_fields = [];
            for ( $i = 1; $i <= $num; $i++ ) {
                $children_fields['child' . $i . '_first_name'] = [
                    'name'        => 'child' . $i . '_first_name',
                    'type'        => 'field-select',
                    'label'       => __( 'Child ' . $i . ' First Name', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'firstname',
                        'textbox',
                    ],
                ];
                $children_fields['child' . $i . '_last_name'] = [
                    'name'        => 'child' . $i . '_last_name',
                    'type'        => 'field-select',
                    'label'       => __( 'Child ' . $i . ' Last Name', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'lastname',
                        'textbox',
                    ],
                ];
                if ( $new_member ) {
                    $children_fields['child' . $i . '_dob'] = [
                        'name'        => 'child' . $i . '_dob',
                        'type'        => 'field-select',
                        'label'       => __( 'Child ' . $i . ' Date of Birth', 'jg_user' ),
                        'width'       => 'full',
                        'group'       => 'primary',
                        'field_types' => [
                            'date',
                        ],
                    ];
                }
                $children_fields['child' . $i . '_shirt_size'] = [
                    'name'        => 'child' . $i . '_shirt_size',
                    'type'        => 'field-select',
                    'label'       => __( 'Child ' . $i . ' Shirt Size', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ];
                if ( $new_member ) {
                    $children_fields['child' . $i . '_indigenous_ancestry'] = [
                        'name'        => 'child' . $i . '_indigenous_ancestry',
                        'type'        => 'field-select',
                        'label'       => __( 'Child ' . $i . ' Indigenous Ancestry', 'jg_user' ),
                        'width'       => 'full',
                        'group'       => 'primary',
                        'field_types' => [
                            'listselect',
                        ],
                    ];
                }
            }

            return $children_fields;
        }

        public static function add_password() {
            return [
                'new_password'     => [
                    'name'        => 'new_password',
                    'type'        => 'field-select',
                    'label'       => __( 'New Password', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'jg_password',
                    ],
                ],
                'current_password' => [
                    'name'        => 'current_password',
                    'type'        => 'field-select',
                    'label'       => __( 'Current Password', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'jg_password',
                    ],
                ],
            ];
        }

        public static function add_profile() {
            return [
                'email'               => [
                    'name'        => 'email',
                    'type'        => 'field-select',
                    'label'       => __( 'Email', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                        'email',
                    ],
                ],
                'username'            => [
                    'name'        => 'username',
                    'type'        => 'field-select',
                    'label'       => __( 'Username', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                    ],
                ],
                'first_name'          => [
                    'name'        => 'first_name',
                    'type'        => 'field-select',
                    'label'       => __( 'First Name', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'firstname',
                        'textbox',
                    ],
                ],
                'last_name'           => [
                    'name'        => 'last_name',
                    'type'        => 'field-select',
                    'label'       => __( 'Last Name', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'lastname',
                        'textbox',
                    ],
                ],
                'dob'                 => [
                    'name'        => 'dob',
                    'type'        => 'field-select',
                    'label'       => __( 'Date of Birth', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'date',
                    ],
                ],
                'shirt_size'          => [
                    'name'        => 'shirt_size',
                    'type'        => 'field-select',
                    'label'       => __( 'Shirt Size', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'street1'             => [
                    'name'        => 'street1',
                    'type'        => 'field-select',
                    'label'       => __( 'Street', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'address',
                    ],
                ],
                'city1'               => [
                    'name'        => 'city1',
                    'type'        => 'field-select',
                    'label'       => __( 'City', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'city',
                    ],
                ],
                'postal_code1'        => [
                    'name'        => 'postal_code1',
                    'type'        => 'field-select',
                    'label'       => __( 'Postal Code', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'textbox',
                    ],
                ],
                'province_territory1' => [
                    'name'        => 'province_territory1',
                    'type'        => 'field-select',
                    'label'       => __( 'Province/Territory', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'indigenous_ancestry' => [
                    'name'        => 'indigenous_ancestry',
                    'type'        => 'field-select',
                    'label'       => __( 'Indigenous Ancestry', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'iswo_region'         => [
                    'name'        => 'iswo_region',
                    'type'        => 'field-select',
                    'label'       => __( 'ISWO Region', 'jg_user' ),
                    'width'       => 'one-half',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
            ];
        }

        public static function add_queries() {
            return [
                'wp_user_id'    => [
                    'name'        => 'wp_user_id',
                    'type'        => 'field-select',
                    'label'       => __( 'WP User ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_user_id' => [
                    'name'        => 'query_user_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query User ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_post_id' => [
                    'name'        => 'query_post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'WP Page ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
            ];
        }

        public static function family() {
            return array_merge( self::add_kids( 1 ), self::add_queries(),
                ['query_submember_id' => [
                    'name'        => 'query_submember_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query Submember ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ]] );
        }

        public static function register_user() {
            return array_merge(
                ['form_type' => [
                    'name'        => 'form_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Youth/Parent Registration', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listradio',
                    ]],
                ], self::add_password(), self::add_profile(), [
                    'how_did_hear'        => [
                        'name'        => 'how_did_hear',
                        'type'        => 'field-select',
                        'label'       => __( 'Program source', 'jg_user' ),
                        'width'       => 'one-half',
                        'group'       => 'primary',
                        'field_types' => [
                            'listselect',
                        ],
                    ],
                    'previous_experience' => [
                        'name'        => 'previous_experience',
                        'type'        => 'field-select',
                        'label'       => __( 'Previous', 'jg_user' ),
                        'width'       => 'one-half',
                        'group'       => 'primary',
                        'field_types' => [
                            'listradio',
                        ],
                    ],
                ], self::add_kids(), self::add_queries() );
        }

        public static function update_password() {
            return array_merge( self::add_password(), self::add_queries() );
        }

        public static function uploads() {
            return [
                'file_upload'   => [
                    'name'        => 'file_upload',
                    'type'        => 'field-select',
                    'label'       => __( 'File Upload', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'file_upload',
                    ],
                ],
                'submember'     => [
                    'name'        => 'submember',
                    'type'        => 'field-select',
                    'label'       => __( 'Family Member', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'listselect',
                    ],
                ],
                'upload_type'   => [
                    'name'        => 'upload_type',
                    'type'        => 'field-select',
                    'label'       => __( 'Upload Type', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'points'        => [
                    'name'        => 'points',
                    'type'        => 'field-select',
                    'label'       => __( 'Points', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'post_id'       => [
                    'name'        => 'post_id',
                    'type'        => 'field-select',
                    'label'       => __( 'WP Post ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'wp_user_id'    => [
                    'name'        => 'wp_user_id',
                    'type'        => 'field-select',
                    'label'       => __( 'WP User ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
                'query_user_id' => [
                    'name'        => 'query_user_id',
                    'type'        => 'field-select',
                    'label'       => __( 'Query User ID', 'jg_user' ),
                    'width'       => 'full',
                    'group'       => 'primary',
                    'field_types' => [
                        'hidden',
                        'number',
                        'textbox',
                    ],
                ],
            ];
        }
    }
}
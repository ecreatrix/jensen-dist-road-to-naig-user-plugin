<?php
namespace jg\Plugin\User;

if ( ! defined( 'ABSPATH' ) ) {
    exit;
}

if ( ! class_exists( NinjaHelpersFields::class ) ) {
    class NinjaHelpersFields {
        public function __construct() {}
        static function error_in_use( $field = 'email' ) {
            return __( 'Unfortunately this ' . $field . ' is already used by somebody else. Please try another one.', 'jg_user' );
        }

        static function get_action_settings( $form_id, $action_id, $config ) {
            $form       = Ninja_Forms()->form( $form_id );
            $action     = $form->get_action( $action_id );
            $field_info = $action->get_settings( array_keys( $config ) );

            return $field_info;
        }

        //Get the field key of each field in the fields array.
        static function get_fields( $form_id, $action_keys, $action_id = false ) {
            $form = Ninja_Forms()->form( $form_id );

            $fields      = $form->get_fields();
            $fields_info = [];

            $action_tags = [];
            if ( $action_id ) {
                $actions = $form->get_actions();
                foreach ( $actions as $action ) {
                    $action_type = $action->get_setting( 'type' );
                    if ( $action_id === $action_type ) {
                        $action_settings = $action->get_settings();

                        $action_tags = array_flip( array_map( function ( $field_key ) {
                            $field_key = str_replace( '{field:', '', $field_key );
                            $field_key = str_replace( '}', '', $field_key );

                            return $field_key;
                        }, $action_settings ) );
                    }
                }

                foreach ( $fields as $field ) {
                    if ( 'confirm' === $field->get_setting( 'type' ) ) {
                        continue;
                    }

                    $key = $field->get_setting( 'key' );
                    $id  = $field->get_id();

                    $value = $field->get_setting( 'value' ) ? $field->get_setting( 'value' ) : $action_keys['_field_' . $id];

                    $action_key = $action_tags[$key];

                    if ( $action_key ) {
                        $fields_info[$action_key] = [
                            'id'    => $id,
                            'value' => $value,
                            'key'   => $key,
                        ];
                    }
                }

                return $fields_info;
            }
            return [];
        }

        public function get_modify_id( $current_type, $modify_type, $modify_id ) {
            $modify_type = $modify_type ? $modify_type : false;
            $modify_id   = isset( $modify_id ) ? $modify_id : false;

            if ( $current_type === $modify_type && false !== $modify_id ) {
                return $modify_id;
            }

            return false;
        }

        //Removes the merge tag formatting
        public static function strip_merge_tags( $settings ) {
            return array_map( function ( $field_key ) {
                $field_key = str_replace( '{field:', '', $field_key );
                $field_key = str_replace( '}', '', $field_key );

                return $field_key;
            }, $settings );
        }
    }
}